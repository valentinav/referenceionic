import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ShowrefPage } from './showref';

@NgModule({
  declarations: [
    ShowrefPage,
  ],
  imports: [
    IonicPageModule.forChild(ShowrefPage),
  ],
})
export class ShowrefPageModule {}
