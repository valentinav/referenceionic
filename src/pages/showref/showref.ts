import { Component } from '@angular/core';
import { AngularFireDatabase } from 'angularfire2/database';
import { ActionSheetController, IonicPage, NavController, NavParams } from 'ionic-angular';
import { ref } from '../../app/models/ref';
import { AddrefPage } from '../addref/addref';
import { ReflistPage } from '../reflist/reflist';

/**
 * Generated class for the ShowrefPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-showref',
  templateUrl: 'showref.html',
  providers: [AngularFireDatabase]
})
export class ShowrefPage {
  reference = {} as ref;
  constructor(public navCtrl: NavController, public navParams: NavParams, public actionSheetController: ActionSheetController, public FBDelete:AngularFireDatabase) {
    this.reference = navParams.data.reference;
    console.log(this.reference);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ShowrefPage');
  }

  async presentActionSheet() {
    const actionSheet = await this.actionSheetController.create({
      cssClass: 'my-custom-class',
      buttons: [{
        text: 'Edit',
        icon: 'create',
        handler: () => {
          console.log('Edit clicked');
          this.navCtrl.push(AddrefPage, {reference:this.reference});
        }
      },{
        text: 'Delete',
        role: 'destructive',
        icon: 'trash',
        handler: () => {
          console.log('Delete clicked');
          this.FBDelete.database.ref('/dataRefs/'+ this.reference.idreferencia).remove();
          this.navCtrl.setRoot(ReflistPage);
        }
      },{
        text: 'Cancel',
        icon: 'close',
        role: 'cancel',
        handler: () => {
          console.log('Cancel clicked');
        }
      }]
    });
    await actionSheet.present();
  }

}
