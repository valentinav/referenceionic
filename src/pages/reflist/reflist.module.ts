import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ReflistPage } from './reflist';

@NgModule({
  declarations: [
    ReflistPage,
  ],
  imports: [
    IonicPageModule.forChild(ReflistPage),
  ],
})
export class ReflistPageModule {}
