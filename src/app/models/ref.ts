export interface ref {
    idreferencia: number;
    titulopub: string;
    autores: string;
    tipopub:number;
    eventorevista: string;
    doi: string;
    anyopub: number;
}